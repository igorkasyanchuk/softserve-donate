class FixContent < ActiveRecord::Migration
  def change
    PagePart.create(identifier: 'email_title_step2', content_en: 'To confirm your will to help the army, please follow the link:', content_uk: 'Для того, щоб підтвердити Ваше бажання допомогти армії, будь ласка, перейдіть за посиланням:', format: 'text')
    PagePart.create(identifier: 'email_body_step2',  content_en: 'Your contribution:', content_uk: 'Ваша пожертва', format: 'text')
  end
end