class AddMonthApprove < ActiveRecord::Migration
  def up
    PagePart.create(identifier: 'monthly_payments', 
      content_en: "I'm also agree to donate such amount of my compensation <br/>for the next ", 
      content_uk: "Я також погоджуюсь жертвувати дану суми моєї компенсації <br/>наступні ", format: 'html')
    PagePart.create(identifier: 'monthly_payments_months', content_en: 'months', content_uk: 'місяці', format: 'text')
    add_column :participants, :monthly_payments, :boolean, default: false
    Participant.update_all monthly_payments: false
  end

  def down
    PagePart.where(identifier: 'monthly_payments').destroy_all
    PagePart.where(identifier: 'monthly_payments_months').destroy_all
  end
end
