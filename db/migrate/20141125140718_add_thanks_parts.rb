class AddThanksParts < ActiveRecord::Migration
  def change
    PagePart.create(identifier: 'confirmation_header', content_en: 'Your contribution has been confirmed.', content_uk: 'Ваш внесок підтверджено.', format: 'text')
    PagePart.create(identifier: 'confirmation_text', content_en: 'Thank you for your help!', content_uk: 'Дякуємо за Вашу небайдужість!', format: 'text')
  end
end
