class AddSectionToSubRepports < ActiveRecord::Migration
  def up
    add_column :sub_reports, :with_section, :boolean
    add_column :sub_reports, :section_en, :text   
    add_column :sub_reports, :section_uk, :text
    change_column :sub_reports, :amount, :integer
    rename_column :sub_reports, :content_en, :short_description_en    
    rename_column :sub_reports, :content_uk, :short_description_uk    
  end

  def down
    remove_column :sub_reports, :with_section, :boolean
    remove_column :sub_reports, :section_en, :text   
    remove_column :sub_reports, :section_uk, :text
    change_column :sub_reports, :amount, :string
    rename_column :sub_reports, :short_description_en, :content_en    
    rename_column :sub_reports, :short_description_uk, :content_uk        
  end
end
