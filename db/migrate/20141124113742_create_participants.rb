class CreateParticipants < ActiveRecord::Migration
  def change
    create_table :participants do |t|
      t.string :name
      t.string :email
      t.string :amount
      t.boolean :confirmed
      t.integer :marathon_id

      t.timestamps
    end
    add_index :participants, [:marathon_id, :confirmed]
  end
end
