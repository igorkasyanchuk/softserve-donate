class AddColumnsToMarathon < ActiveRecord::Migration
  def change
    add_column :marathons, :voting_active, :boolean
    add_column :marathons, :money_amount_en, :integer, default: 0
    add_column :marathons, :money_amount_uk, :integer, default: 0

    Marathon.update_all money_amount_uk: 0
    Marathon.update_all money_amount_en: 0
  end
end
