class AddPlaceholders < ActiveRecord::Migration
  def change
    PagePart.create(identifier: 'user_placeholder', content_en: 'First Name and Last Name', content_uk: 'Ім’я та прізвище', format: 'text')
    PagePart.create(identifier: 'email_placeholder', content_en: 'Please enter email', content_uk: 'Введіть, будь ласка свій email', format: 'text')
  end
end
