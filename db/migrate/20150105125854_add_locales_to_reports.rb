class AddLocalesToReports < ActiveRecord::Migration
  def change
    rename_column :sub_reports, :content, :content_en
    add_column :sub_reports, :content_uk, :text
    rename_column :city_reports, :content, :content_en
    add_column :city_reports, :content_uk, :text
  end
end
