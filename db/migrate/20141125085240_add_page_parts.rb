class AddPageParts < ActiveRecord::Migration
  def change
    PagePart.all.each {|part| part.destroy}
    PagePart.create(identifier: 'main_header', content_en: 'Help Ukrainian Army', content_uk: 'Допоможи Українській Армії', format: 'text')
    PagePart.create(identifier: 'form_header', content_en: 'Show your support for the army and contribute:', content_uk: 'Підтримай наших, зроби внесок:', format: 'text')
    PagePart.create(identifier: 'radio_1', content_en: '1 day', content_uk: '1 день', format: 'text')
    PagePart.create(identifier: 'radio_2', content_en: '2 days', content_uk: '2 дні', format: 'text')
    PagePart.create(identifier: 'radio_3', content_en: '3 days', content_uk: '3 дні', format: 'text')
    PagePart.create(identifier: 'radio_1_comment', content_en: '(compensation for 1 working day)', content_uk: '(компенсація за 1 робочий день)', format: 'text')
    PagePart.create(identifier: 'radio_2_comment', content_en: '(compensation 2 working days)', content_uk: '(компенсація за 2 робочі дні)', format: 'text')
    PagePart.create(identifier: 'radio_3_comment', content_en: '(compensation for 3 working days)', content_uk: '(компенсація за 3 робочі дні)', format: 'text')
    PagePart.create(identifier: 'amount', content_en: '(specify the amount, in USD)', content_uk: '(вкажіть суму в доларах)', format: 'text')
    PagePart.create(identifier: 'toc_description', content_en: 'By choosing one of the above options, I give my consent to use the monetary equivalent of the specified number of days from my November compensation or the specified amount of money to purchase necessities to support the Ukrainian army.', content_uk: 'Вибравши одну із опцій я надаю згоду використати еквівалент вказаної кількості днів моєї місячної компенсації за листопад, або вказану мною суму коштів на придбання необхідних речей для підтримки української армії.', format: 'text')

    PagePart.create(identifier: 'open_modal', content_en: 'CONTRIBUTE', content_uk: 'ЗРОБИТИ ВНЕСОК', format: 'text')
    PagePart.create(identifier: 'close', content_en: 'Close', content_uk: 'Закрити', format: 'text')
    PagePart.create(identifier: 'modal_title', content_en: 'Enter your name and email', content_uk: "Введіть своє ім'я та email", format: 'text')
    PagePart.create(identifier: 'form_name', content_en: 'Name and surname', content_uk: "Ім'я та прізвище", format: 'text')
    PagePart.create(identifier: 'form_email', content_en: 'Email(SoftServe inc.)', content_uk: "Email(SoftServe inc.)", format: 'text')
    PagePart.create(identifier: 'submit', content_en: 'Сontribute', content_uk: 'Зробити внесок', format: 'text')
    PagePart.create(identifier: 'progress_header', content_en: 'of SoftServe employees', content_uk: 'Працівників', format: 'text')
    PagePart.create(identifier: 'progress_text', content_en: 'have already contributed to the fundraising.', content_uk: 'SoftServe вже долучились до збору коштів', format: 'text')
    PagePart.create(identifier: 'progress_time_left', content_en: 'Remaining', content_uk: 'Залишилось', format: 'text')
    PagePart.create(identifier: 'products_header', content_en: 'What the collected money will be spent on:', content_uk: 'На що будуть витрачені зібрані кошти', format: 'text')
    PagePart.create(identifier: 'footer_text', content_en: 'Благодійний фонд "Відкриті Очі"', content_uk: 'Благодійний фонд "Відкриті Очі"', format: 'text')
    PagePart.create(identifier: 'email_sended', content_en: 'The <span class="yellow">confirmation</span> has been sent to your email address.', content_uk: 'На вашу електронну адресу був надісланий <span class="yellow">лист-підтвердження</span>', format: 'html')
    PagePart.create(identifier: 'email_sended_details', content_en: 'To confirm your desire to help the army, <span class="yellow">follow the link</span> in the email.', content_uk: 'Для того ,щоб підтвердити ваше бажання допомогти армії, будь ласка <span class="yellow">перейдіть за посиланням у листі</span>', format: 'html')
    PagePart.create(identifier: 'title_main', content_en: 'SoftServe Donate', content_uk: 'SoftServe Donate', format: 'text')
  end
end
