class AddTranslationForPopupAndLastPage < ActiveRecord::Migration
  def change
    html_en = "<h1>Your contribution has been confirmed.</h1><p>Дякуємо за Вашу небайдужість!</p><p>Розкажіть про збір коштів своїм колегам.</p>"
    html_uk = "<h1>Ваш внесок підтверджено.</h1><p>Thank you for your help!</p><p>Share the information about fundraising with colleagues.</p>"
    PagePart.create(identifier: 'confirmation_page_text', content_en: html_en, content_uk: html_uk, format: 'html')
  end
end
