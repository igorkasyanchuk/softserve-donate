class AddIndexes < ActiveRecord::Migration
  def change
    add_index :sub_reports, :report_id
    add_index :city_reports, :report_id
  end
end
