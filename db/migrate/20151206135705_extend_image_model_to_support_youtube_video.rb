class ExtendImageModelToSupportYoutubeVideo < ActiveRecord::Migration
  def change
    add_column :images, :youtube_url, :string, limit: 2083
    add_column :images, :asset_type, :integer, null: false, default: 0
  end
end
