class AddBucketsToReports < ActiveRecord::Migration
  def change
    add_column :reports, :bucket, :boolean, default: false
  end
end
