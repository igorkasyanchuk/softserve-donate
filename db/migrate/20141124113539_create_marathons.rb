class CreateMarathons < ActiveRecord::Migration
  def change
    create_table :marathons do |t|
      t.string :name
      t.date :finishing_on
      t.boolean :active, default: false

      t.timestamps
    end
    add_index :marathons, :active
  end
end
