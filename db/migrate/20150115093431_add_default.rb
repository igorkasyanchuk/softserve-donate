class AddDefault < ActiveRecord::Migration
  def up
    change_column_default :reports, :money_uah, 0
    change_column_default :reports, :money_usd, 0

    Report.all.each do |report|
      unless report.money_usd
        report.money_usd = 0 
      end
      unless report.money_uah
        report.money_uah = 0 
      end
      report.save
    end 

  end

  def down
    change_column_default :reports, :money_uah, nil
    change_column_default :reports, :money_usd, nil
  end
end
