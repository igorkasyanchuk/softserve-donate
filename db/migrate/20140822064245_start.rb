class Start < ActiveRecord::Migration
  def change


    create_table "categories", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ckeditor_assets", force: true do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.integer  "assetable_id"
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width"
    t.integer  "height"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "contacts", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.string   "attachment"
    t.boolean  "processed"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "contact_type"
    t.string   "organization_name"
    t.text     "description"
  end

  create_table "credits", force: true do |t|
    t.integer  "project_id"
    t.decimal  "amount",           precision: 10, scale: 0
    t.integer  "user_id"
    t.string   "user_name"
    t.text     "description"
    t.string   "money_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "currency",                                  default: "UAH"
    t.decimal  "original_amount",  precision: 10, scale: 0
    t.date     "transaction_date"
  end

  add_index "credits", ["project_id"], name: "index_credits_on_project_id", using: :btree

  create_table "documents", force: true do |t|
    t.string   "document"
    t.integer  "documentable_id"
    t.string   "documentable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", force: true do |t|
    t.date     "event_date"
    t.string   "title"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "organization_id"
    t.string   "preview"
  end

  add_index "events", ["organization_id"], name: "index_events_on_organization_id", using: :btree

  create_table "organizations", force: true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "logo"
    t.text     "description"
    t.string   "phone_1"
    t.string   "phone_2"
    t.string   "email"
    t.string   "website"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "organizations", ["user_id"], name: "index_organizations_on_user_id", using: :btree

  create_table "page_parts", force: true do |t|
    t.string   "identifier"
    t.text     "content_en"
    t.text     "content_uk"
    t.string   "format"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "page_parts", ["identifier"], name: "index_page_parts_on_identifier", using: :btree

  create_table "pages", force: true do |t|
    t.text     "content",         limit: 2147483647
    t.string   "seo_title"
    t.text     "seo_keywords"
    t.text     "seo_description"
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.integer  "parent_id"
    t.integer  "priority",                           default: 0
    t.string   "position",                           default: "header"
  end

  create_table "photos", force: true do |t|
    t.string   "photo"
    t.integer  "photable_id"
    t.string   "photable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", force: true do |t|
    t.string   "name"
    t.decimal  "money_required",  precision: 10, scale: 0
    t.decimal  "money_collected", precision: 10, scale: 0
    t.string   "description"
    t.boolean  "is_completed"
    t.string   "photo"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "projects", force: true do |t|
    t.integer  "organization_id"
    t.string   "name"
    t.text     "description"
    t.string   "image"
    t.integer  "amount"
    t.boolean  "completed",       default: false
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "preview"
    t.boolean  "featured",        default: false
  end

  add_index "projects", ["category_id"], name: "index_projects_on_category_id", using: :btree
  add_index "projects", ["organization_id"], name: "index_projects_on_organization_id", using: :btree

  create_table "sites", force: true do |t|
    t.string   "contact_email"
    t.text     "js_code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "fb"
    t.string   "gp"
    t.string   "vk"
    t.string   "tw"
  end

  create_table "usages", force: true do |t|
    t.integer  "project_id"
    t.decimal  "amount",           precision: 10, scale: 0
    t.integer  "user_id"
    t.string   "user_name"
    t.text     "description"
    t.string   "money_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "currency",                                  default: "UAH"
    t.decimal  "original_amount",  precision: 10, scale: 0
    t.date     "transaction_date"
  end

  add_index "usages", ["project_id"], name: "index_usages_on_project_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "role"
    t.integer  "organization_id"
    t.string   "first_name"
    t.string   "last_name"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["organization_id"], name: "index_users_on_organization_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  
  end
end
