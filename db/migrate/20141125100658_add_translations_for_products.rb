class AddTranslationsForProducts < ActiveRecord::Migration
  def change
    rename_column :products, :name, :name_en
    add_column :products, :name_uk, :string

    rename_column :products, :description, :description_en
    add_column :products, :description_uk, :text
  end
end
