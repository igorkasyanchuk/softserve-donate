class CreateReportTable < ActiveRecord::Migration
  def change
    create_table :report_tables do |t|
      t.string :name_en
      t.string :name_uk
      t.string :amount_en
      t.string :amount_uk
      t.float  :price
      t.references :tableable, polymorphic: true, index: true
      t.timestamps null: false
    end
  end
end
