class AddNameToReport < ActiveRecord::Migration
  def change
    add_column :reports, :name_en,  :string
    add_column :reports, :name_uk,  :string
    add_column :reports, :end_month, :date
  end
end
