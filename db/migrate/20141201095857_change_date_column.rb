class ChangeDateColumn < ActiveRecord::Migration
  def change
    change_column :marathons, :finishing_on, :datetime
    Marathon.update_all :finishing_on => "2014-12-1 18:00"
  end
end
