class AddMonthValues < ActiveRecord::Migration
  def change
    add_column :participants, :months, :integer, default: 0
    Participant.update_all months: 0
  end
end
