class AddFinalPercentage < ActiveRecord::Migration
  def change
    add_column :marathons, :final_percentage, :integer, default: 0
  end
end
