class CreateProducts < ActiveRecord::Migration
  def change
    drop_table :products
    create_table :products do |t|
      t.string :name
      t.string :image
      t.text :description

      t.timestamps
    end
    create_table :marathons_products, id: false do |t|
      t.integer :marathon_id
      t.integer :product_id
    end
    add_index :marathons_products, :marathon_id
    add_index :marathons_products, :product_id
  end
end
