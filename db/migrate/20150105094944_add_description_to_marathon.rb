class AddDescriptionToMarathon < ActiveRecord::Migration
  def change
    add_column :reports, :description_en, :string
    add_column :reports, :description_uk, :string
  end
end
