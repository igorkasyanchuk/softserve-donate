class FixPp < ActiveRecord::Migration
  def change
    p = PagePart.find_by identifier: 'progress_header'
    p.content_en = "<p class=\"strong\">XXX of YYY SoftServe employees</p>\r\n<p>have already contributed to the fundraising.</p>"
    p.content_uk = "<p class=\"strong\">XXX із YYY працівників SoftServe</p>\r\n<p>вже долучились до збору коштів.</p>"
    p.save!
  end
end
