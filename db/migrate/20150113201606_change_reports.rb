class ChangeReports < ActiveRecord::Migration
  def change
    add_column :reports, :report_month, :date
    add_column :reports, :money_uah, :integer
    add_column :reports, :money_usd, :integer
    remove_column :reports, :marathon_id, :integer
  end
end
