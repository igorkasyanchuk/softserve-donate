class AddPeopleNumberToReport < ActiveRecord::Migration
  def change
    add_column :reports, :final_number_of_people, :integer
  end
end
