class CreateSubRerort < ActiveRecord::Migration
  def change
    create_table :sub_reports do |t|
      t.string :amount
      t.text :content
      t.integer :report_id
      t.string :image
    end
  end
end
