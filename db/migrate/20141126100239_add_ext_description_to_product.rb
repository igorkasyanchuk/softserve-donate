class AddExtDescriptionToProduct < ActiveRecord::Migration
  def change
    add_column :products, :ext_description_uk, :text
    add_column :products, :ext_description_en, :text
  end
end
