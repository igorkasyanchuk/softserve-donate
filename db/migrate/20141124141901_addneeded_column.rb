class AddneededColumn < ActiveRecord::Migration
  def change
    add_column :participants, :payment_type, :string
    add_column :participants, :toc, :boolean
  end
end
