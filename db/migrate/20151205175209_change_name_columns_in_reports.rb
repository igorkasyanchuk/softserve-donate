class ChangeNameColumnsInReports < ActiveRecord::Migration
  def up
    %i( reports report_tables ).each do |table|
      change_column table, :name_uk, :string, limit: 512
      change_column table, :name_en, :string, limit: 512
    end
  end

  def down
    %i( reports report_tables ).each do |table|
      change_column table, :name_uk, :string
    end
  end
end
