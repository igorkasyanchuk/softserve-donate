class MakeImagePolymorphic < ActiveRecord::Migration
  def up
    rename_column :images, :city_report_id, :imageable_id
    add_column :images, :imageable_type, :string
    add_index :images, :imageable_id
    process_city_association!
  end

  def down
    restore_city_association!
    remove_index :images, :imageable_id
    remove_column :images, :imageable_type
    rename_column :images, :imageable_id, :city_report_id
  end

  private

  def process_city_association!
    Image.find_in_batches { |batch| batch.map { |r| r.update_attributes(imageable_type: 'CityReport') } }
  end

  def restore_city_association!
    Image.where.not(imageable_type: 'CityReport').destroy_all
  end
end
