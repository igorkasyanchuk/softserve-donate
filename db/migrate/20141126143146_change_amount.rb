class ChangeAmount < ActiveRecord::Migration
  def change
    Participant.destroy_all
    change_column :participants, :amount, :decimal
  end
end
