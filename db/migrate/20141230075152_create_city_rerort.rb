class CreateCityRerort < ActiveRecord::Migration
  def change
    create_table :city_reports do |t|
      t.string :city
      t.text :content
      t.integer :report_id
    end
  end
end
