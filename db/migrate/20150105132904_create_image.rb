class CreateImage < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :image
      t.string :description_en
      t.string :description_uk
      t.integer :city_report_id
    end
  end
end
