class AddCurrencyColumnToCityReports < ActiveRecord::Migration
  def change
    add_column :city_reports, :currency, :integer, null: false, default: 0
  end
end
