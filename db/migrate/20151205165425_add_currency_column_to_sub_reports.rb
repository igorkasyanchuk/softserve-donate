class AddCurrencyColumnToSubReports < ActiveRecord::Migration
  def change
    add_column :sub_reports, :currency, :integer, null: false, default: 0
  end
end
