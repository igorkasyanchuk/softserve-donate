class FinalNumberOfEmploees < ActiveRecord::Migration
  def change
    add_column :marathons, :final_number_of_people, :integer, default: 0
  end
end
