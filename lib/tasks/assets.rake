namespace :assets do
  namespace :images do
    desc 'Reprocess all images in Image, SubReport, Event, Product, Project, Photo models.'
    task reprocess: :environment do
      puts 'Start processing..'
      {
        image: :image,
        event: :preview,
        photo: :photo,
        product: :image,
        project: :preview,
        sub_report: :image
      }.each_pair do |key, attachment|
        model = key.to_s.classify.constantize
        print model
        model.find_in_batches do |batch|
          batch.each do |record|
            begin
              next unless record.respond_to?(attachment)
              record.try(attachment).recreate_versions!
              print '.'
            rescue Errno::ENOENT
              print '*'
            end
          end
        end
        puts
      end
    end
  end
end
