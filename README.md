# README

### Background video player

* Dynamic configurations

  * visit admin content page
  * create or edit following options:
    
    > Identifier: 'youtube_bg_video_url' - url to youtube video
    > Identifier: 'youtube_bg_video_start_at' - (in sec) point to start video from
    > Identifier: 'youtube_bg_video_stop_at' - (in sec) point to stop at. Set '0' or blank to play to end.

### Marathon timer logic

#### For proper work of countdown timer follow this instruction

  * Properly set of start and finish dates of marathon
