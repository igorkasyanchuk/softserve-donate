base_url = "http://#{request.host_with_port}"
xml.instruct! :xml, :version => '1.0'

xml.tag! 'urlset', "xmlns" => "http://www.sitemaps.org/schemas/sitemap/0.9" do

  xml.url do
    xml.loc "#{base_url}"
    xml.changefreq "monthly"
    xml.priority 1.0
  end

  xml.url do
    xml.loc "#{base_url}/contacts/new"
    xml.lastmod Time.now.to_date
    xml.changefreq "monthly"
    xml.priority 1.0
  end

  xml.url do
    xml.loc events_url
    xml.lastmod Time.now.to_date
    xml.changefreq "monthly"
    xml.priority 1.0
  end

  xml.url do
    xml.loc projects_url
    xml.lastmod Time.now.to_date
    xml.changefreq "monthly"
    xml.priority 1.0
  end

  @pages.each do |e|
    xml.url do
      xml.loc page_url(e)
      xml.lastmod e.updated_at.to_date
      xml.changefreq "always"
      xml.priority 0.9
    end
  end

  @projects.each do |project|
    xml.url do
      xml.loc project_url(project)
      xml.lastmod project.updated_at.to_date
      xml.changefreq "always"
      xml.priority 0.9
    end
  end

  @events.each do |events|
    xml.url do
      xml.loc event_url(events)
      xml.lastmod events.updated_at.to_date
      xml.changefreq "always"
      xml.priority 0.9
    end
  end

end
