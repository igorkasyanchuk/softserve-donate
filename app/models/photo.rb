# == Schema Information
#
# Table name: photos
#
#  id            :integer          not null, primary key
#  photo         :string(255)
#  photable_id   :integer
#  photable_type :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#

class Photo < ActiveRecord::Base

  belongs_to :project
  belongs_to :usage
  belongs_to :city_report

  mount_uploader :photo, PhotoUploader
end
