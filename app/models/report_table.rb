# == Schema Information
#
# Table name: report_tables
#
#  id             :integer          not null, primary key
#  name_en        :string(255)
#  name_uk        :string(255)
#  amount_en      :string(255)
#  amount_uk      :string(255)
#  price          :float(24)
#  tableable_id   :integer
#  tableable_type :string(255)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class ReportTable < ActiveRecord::Base
  belongs_to :tableable, polymorphic: true
  validates :name_uk, :name_en, :price, presence: true

  def name
    send("name_#{I18n.locale}")
  end

  def amount
    send("amount_#{I18n.locale}")
  end

end
