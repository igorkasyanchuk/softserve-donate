# == Schema Information
#
# Table name: images
#
#  id             :integer          not null, primary key
#  image          :string(255)
#  description_en :string(255)
#  description_uk :string(255)
#  city_report_id :integer
#

class Image < ActiveRecord::Base
  belongs_to :imageable, polymorphic: true

  enum asset_type: { image: 0, youtube_embed: 1 }

  mount_uploader :image, PhotoUploader

  before_validation do
    self.asset_type = self.class.asset_types[:youtube_embed] if youtube_url.present?
  end

  validates :asset_type, presence: true, inclusion: { in: asset_types.keys }
  validates :youtube_url, allow_blank: true, format: { with: %r{\Ahttps?://[w]{3}.youtube.com/embed/.+\z}i }

  def description
    send("description_#{I18n.locale}")
  end
end
