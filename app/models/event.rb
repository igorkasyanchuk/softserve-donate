# == Schema Information
#
# Table name: events
#
#  id              :integer          not null, primary key
#  event_date      :date
#  title           :string(255)
#  description     :text
#  created_at      :datetime
#  updated_at      :datetime
#  organization_id :integer
#  preview         :string(255)
#

class Event < ActiveRecord::Base
  default_scope -> {order("event_date desc")}

  mount_uploader :preview, PhotoUploader

  belongs_to :organization

  validates :title, :organization, :event_date, :description, presence: true

  has_many :photos,  as: :photable, dependent: :destroy
  accepts_nested_attributes_for :photos,    allow_destroy: true, reject_if: proc { |attributes| attributes['photo'].blank? }

  def to_param
    "#{self.id}-#{self.title}".mb_chars.downcase.gsub(/[^a-z0-9а-яії]+/i, '-').transliterate
  end

  def short_description
    Nokogiri::HTML(self.description).text.truncate(270, ommision: "...")
  end
end
