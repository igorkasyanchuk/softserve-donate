# == Schema Information
#
# Table name: documents
#
#  id                :integer          not null, primary key
#  document          :string(255)
#  documentable_id   :integer
#  documentable_type :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#

class Document < ActiveRecord::Base

  belongs_to :project
  belongs_to :usage

  mount_uploader :document, DocumentUploader


  def ui_file_name
    File.basename(self.document.path)
  end

end
