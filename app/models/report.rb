# == Schema Information
#
# Table name: reports
#
#  id                     :integer          not null, primary key
#  created_at             :datetime
#  updated_at             :datetime
#  description_en         :string(255)
#  description_uk         :string(255)
#  report_month           :date
#  money_uah              :integer          default(0)
#  money_usd              :integer          default(0)
#  final_number_of_people :integer
#  name_en                :string(255)
#  name_uk                :string(255)
#  end_month              :date
#  bucket                 :boolean          default(FALSE)
#

class Report < ActiveRecord::Base
  has_many :sub_reports, dependent: :destroy
  has_many :city_reports, dependent: :destroy

  accepts_nested_attributes_for :sub_reports, allow_destroy: true
  accepts_nested_attributes_for :city_reports, allow_destroy: true

  scope :ordered, -> { reorder(created_at: :desc) }
  scope :without_bucket, -> { where(bucket: false) }
  scope :with_bucket, -> { where(bucket: true) }

  def self.create_default
    create(bucket: true, name_uk: 'Кошти зі скриньок', report_month: Date.current - 6.month, end_month: Date.current)
  end

  def description
    try("description_#{I18n.locale}")
  end

  def name
    try("name_#{I18n.locale}")
  end

  def self.bucket
    with_bucket.first
  end
end
