# == Schema Information
#
# Table name: projects
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  name            :string(255)
#  description     :text
#  image           :string(255)
#  amount          :integer
#  completed       :boolean          default(FALSE)
#  category_id     :integer
#  created_at      :datetime
#  updated_at      :datetime
#  preview         :string(255)
#  featured        :boolean          default(FALSE)
#

class Project < ActiveRecord::Base

  mount_uploader :preview, PhotoUploader

  scope :completed, -> {where(:completed => true)}
  scope :open,      -> {where(:completed => false)}
  scope :featured,  -> {where(:featured  => true)}
  scope :random,    -> {order("rand()")}
  scope :by_completed, -> {order(:completed)}

  belongs_to :organization
  belongs_to :category

  has_many :credits, dependent: :destroy
  has_many :usages,  dependent: :destroy
  has_many :photos,  as: :photable, dependent: :destroy
  has_many :documents, as: :documentable, dependent: :destroy

  accepts_nested_attributes_for :photos,    allow_destroy: true, reject_if: proc { |attributes| attributes['photo'].blank? }
  accepts_nested_attributes_for :documents, allow_destroy: true, reject_if: proc { |attributes| attributes['document'].blank? }

  validates :name, :organization, :amount, :description, presence: true

  def self.received
    all.map(&:total_received).sum
  end

  def self.spent
    all.map(&:total_spent).sum
  end

  def self.need
    open.all.map(&:amount).sum - open.received
  end

  def to_param
    "#{self.id}-#{self.name}".mb_chars.downcase.gsub(/[^a-z0-9а-яії]+/i, '-').transliterate
  end

  def total_received
    self.credits.map(&:amount).sum
  end

  def total_spent
    self.usages.map(&:amount).sum
  end

  def available
    a = total_received - total_spent
    a > 0 ? a : 0
  end

  def percent_complete
    (self.total_received * 100) / amount rescue 0
  end

  def ui_percent_complete
    [self.percent_complete, 100].min rescue 0
  end

  def short_description
    Nokogiri::HTML(self.description).text.truncate(270, ommision: "...")
  end

  def order_id
    "project##{self.id}##{Time.now.to_i}"
  end

  def active?
    !self.completed
  end

  def money_collected?
    total_received >= amount
  end

end
