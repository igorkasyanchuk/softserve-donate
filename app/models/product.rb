# == Schema Information
#
# Table name: products
#
#  id                 :integer          not null, primary key
#  name_en            :string(255)
#  image              :string(255)
#  description_en     :text
#  created_at         :datetime
#  updated_at         :datetime
#  name_uk            :string(255)
#  description_uk     :text
#  ext_description_uk :text
#  ext_description_en :text
#

class Product < ActiveRecord::Base
  validates :name_en, :name_uk, presence: true

  has_and_belongs_to_many :marathons

  mount_uploader :image, PhotoUploader

  def name
    send("name_#{I18n.locale}")
  end

  def description
    send("description_#{I18n.locale}")
  end

  def ext_description
    send("ext_description_#{I18n.locale}")
  end
end
