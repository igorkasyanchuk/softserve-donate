# == Schema Information
#
# Table name: marathons
#
#  id                     :integer          not null, primary key
#  name                   :string(255)
#  finishing_on           :datetime
#  active                 :boolean          default(FALSE)
#  created_at             :datetime
#  updated_at             :datetime
#  voting_active          :boolean
#  money_amount_en        :integer          default(0)
#  money_amount_uk        :integer          default(0)
#  final_percentage       :integer          default(0)
#  final_number_of_people :integer          default(0)
#  started_at             :datetime
#

class Marathon < ActiveRecord::Base
  TOTAL_PEOPLE = 4042

  has_many :participants
  has_and_belongs_to_many :products

  scope :ordered, -> { order(:id) }
  scope :reordered_id, -> { reorder(id: :desc) }
  scope :active, -> { where(active: true) }
  scope :inactive, -> { where(active: false) }
  scope :not_finished, -> { where('`finishing_on` >= ?', Time.zone.now) }
  scope :started, -> { where('`started_at` <= ?', Time.zone.now) }
  scope :not_started, -> { where('`started_at` > ?', Time.zone.now) }
  scope :finished, -> { where('`started_at` < :date AND `finishing_on` < :date', { date: Time.zone.now }) }
  scope :with_start_interval, ->(left, right) { where(started_at: left..right) }
  scope :with_finish_interval, ->(left, right) { where(finishing_on: left..right) }

  validates :name, presence: true, length: { maximum: 255 }
  validates :finishing_on, presence: true

  def self.current
    active.ordered.last
  end

  def self.active_or_timed
    reordered_id.active.started.not_finished.limit(1)
      .union(ordered.not_started.not_finished.with_start_interval(Time.zone.now, Time.zone.now + 1.week).limit(1))
      .union(ordered.finished.with_finish_interval(Time.zone.now - 1.week, Time.zone.now).limit(1))
      .first
  end

  def timed?
    held? || will_be?
  end

  def held?(interval = 0)
    finishing_on >= Time.zone.now - interval && finishing_on < Time.zone.now
  end

  def in_a_week?
    in_a?(1.week)
  end

  def in_a?(interval)
    available? && started_at <= Time.zone.now + interval
  end

  def available?
    finishing_on >= Time.zone.now
  end

  def in_progress?
    active? && available? && started_at <= Time.zone.now
  end

  def about_to_start?
    active? && available? && started_at > Time.zone.now
  end

  def voting?
    available? && voting_active?
  end

  def progress
    [confirmed_participants_percent.ceil, 100].min
  end

  def confirmed_participants_percent
    100 * participants.confirmed.count.to_f / TOTAL_PEOPLE
  end

  def grouped_participants
    group_participants_by_date.count.sort_by { |k, _v| k }.collect { |value| [value[0].to_datetime.to_i*1000, value[1]] }
  end

  def group_participants_by_date
    participants.group("date_format(`created_at`, '%Y%m%d%H')")
  end

  def self.for_select
    ordered.select(:id, :name).map { |e| [e.name, e.id] }
  end

  def next_marathon
    self.class.where('id > ?', id).first || create_default
  end

  def create_default
    Marathon.create(name: 'Next Report', started_at: DateTime.current + 1.month, finishing_on: DateTime.current + 2.month)
  end
end
