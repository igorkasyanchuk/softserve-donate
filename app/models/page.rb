# -*- encoding : utf-8 -*-
# == Schema Information
#
# Table name: pages
#
#  id              :integer          not null, primary key
#  content         :text(2147483647)
#  seo_title       :string(255)
#  seo_keywords    :text
#  seo_description :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  parent_id       :integer
#  priority        :integer          default(0)
#  position        :string(255)      default("header")
#

class Page < ActiveRecord::Base
  POSITIONS = ['header', 'footer', 'none']

  has_many :child_pages, :class_name => 'Page', :foreign_key => :parent_id
  belongs_to :parent, :class_name => 'Page', :foreign_key => :parent_id

  validates :seo_title, :content, presence: true
  validates_inclusion_of :position, :in => POSITIONS

  scope :root_pages,    -> { where(parent_id: nil)         }
  scope :by_priority,   -> { order('priority desc')        }
  scope :for_header,    -> { where(:position => 'header')  }
  scope :for_footer,    -> { where(:position => 'footer')  }
  scope :about,         -> { where(:seo_title => 'About')  }
  scope :except_about,  -> { where("seo_title <> 'About'") }

  alias_attribute :title, :seo_title

  def to_param
    [id, seo_title.presence].compact.join('-').mb_chars.downcase.gsub(/[^a-z0-9а-яії]+/i, '-')
  end

  def root?
    self.parent_id.nil?
  end

  def with_sub_pages?
    root? && child_pages.any?
  end

end
