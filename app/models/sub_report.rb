# == Schema Information
#
# Table name: sub_reports
#
#  id                   :integer          not null, primary key
#  amount               :integer
#  short_description_en :text
#  report_id            :integer
#  image                :string(255)
#  short_description_uk :text
#  with_section         :boolean
#  section_en           :text
#  section_uk           :text
#

class SubReport < ActiveRecord::Base
  belongs_to :report
  has_many :report_tables, as: :tableable, :dependent => :destroy
  has_many :images, as: :imageable, dependent: :destroy

  accepts_nested_attributes_for :report_tables, allow_destroy: true
  accepts_nested_attributes_for :images, allow_destroy: true

  enum currency: { uah: 0, usd: 1 }

  mount_uploader :image, PhotoUploader

  validates :image, :short_description_uk, :short_description_en, :amount, presence: true
  validates :currency, presence: true, inclusion: { in: currencies.keys }

  scope :with_section, -> { where(with_section: true) }

  def self.currencies_for_select

  end

  def short_description
    send("short_description_#{I18n.locale}")
  end

  def section
    send("section_#{I18n.locale}")
  end
end
