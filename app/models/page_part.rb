# == Schema Information
#
# Table name: page_parts
#
#  id         :integer          not null, primary key
#  identifier :string(255)
#  content_en :text
#  content_uk :text
#  format     :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class PagePart < ActiveRecord::Base
  validates :identifier, presence: true
  validates :identifier, length: {in: 3..25}

  def content
    send("content_#{I18n.locale}")
  end

end