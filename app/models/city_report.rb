# == Schema Information
#
# Table name: city_reports
#
#  id         :integer          not null, primary key
#  city       :string(255)
#  content_en :text
#  report_id  :integer
#  content_uk :text
#

class CityReport < ActiveRecord::Base
  has_many :report_tables, as: :tableable, :dependent => :destroy
  belongs_to :report
  has_many :images, as: :imageable

  enum currency: { uah: 0, usd: 1 }

  accepts_nested_attributes_for :images, :allow_destroy => true
  accepts_nested_attributes_for :report_tables, :allow_destroy => true

  CITIES = [[I18n.t(:ivano_frankivsk) , :ivano_frankivsk], [I18n.t(:lviv) , :lviv], [I18n.t(:kiyv) , :kiyv],
            [I18n.t(:dnipropetrovsk) , :dnipropetrovsk], [I18n.t(:rivne) , :rivne], [I18n.t(:harkiv) , :harkiv],
            [I18n.t(:chernivci) , :chernivci], [I18n.t(:summary) , :summary]]

  validates :city, :content_uk, :content_en, presence: true
  validates :currency, presence: true, inclusion: { in: currencies.keys }

  def content
    try("content_#{I18n.locale}".to_sym)
  end
end
