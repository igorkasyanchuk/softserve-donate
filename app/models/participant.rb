# == Schema Information
#
# Table name: participants
#
#  id               :integer          not null, primary key
#  name             :string(255)
#  email            :string(255)
#  amount           :integer
#  confirmed        :boolean
#  marathon_id      :integer
#  created_at       :datetime
#  updated_at       :datetime
#  payment_type     :string(255)
#  toc              :boolean
#  code             :string(255)
#  monthly_payments :boolean          default(FALSE)
#

class Participant < ActiveRecord::Base
  include HomeHelper
  include ActionView::Helpers::NumberHelper

  belongs_to :marathon

  after_initialize :set_default_option, :if => :new_record?
  def set_default_option
    self.payment_type ||= 'option_1'
  end

  default_scope     -> { order(:email) }
  scope :confirmed, -> { where(confirmed: true) }
  scope :pending,   -> { where(confirmed: false) }

  validates_format_of    :email, :with  => User::VALID_EMAIL_REGEX, :allow_blank => true, :if => :email_changed?
  validates_inclusion_of :payment_type, :in => %w(option_1 option_2 option_3 other)

  validates_acceptance_of :toc, :accept => true, :message => "You must accept the terms of service"
  validates :name, :email, presence: true
  validates :amount, presence: true, if: :validate_if_other?
  validates :amount, :numericality => { :greater_than => 0 }, if: :validate_if_other?

  before_create :generate_code
  after_create :deliver_confirmation_notification

  def generate_code
    begin
      self.code = SecureRandom.hex(32)
    end while self.class.exists?(code: self.code)
  end

  def confirm!
    unless self.confirmed?
      self.confirmed = true
      save!
      Notification.confirmed(self).deliver
    end
  end

  def deliver_confirmation_notification
    Notification.confirm(self).deliver
  end

  def select_option_human
    case self.payment_type
    when 'option_1'
      "#{page_part("radio_1")} #{page_part('radio_1_comment')}"
    when 'option_2'
      "#{page_part("radio_2")} #{page_part('radio_2_comment')}"
    when 'option_3'
      "#{page_part("radio_3")} #{page_part('radio_3_comment')}"
    else
      "$#{self.amount}"
    end
  end

  def validate_if_other?
    payment_type == "other"
  end

  def confirmed_human
    self.confirmed ? 'yes' : 'no'
  end

  def monthly_payments_human
    if self.monthly_payments && self.months > 0
      "#{I18n.t('duration')} #{I18n.t("month_#{self.months}")}"
    end
  end

  comma do
   name
   email
   select_option_human
   confirmed_human
   monthly_payments
   months
   created_at { |e| e.to_date }
  end

end
