# -*- encoding : utf-8 -*-
class PagesController < ApplicationController

  def show
    @page = Page.find params[:id]
    @root = @page.parent || @page
  end

end
