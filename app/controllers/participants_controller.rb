class ParticipantsController < ApplicationController

  def create
    @marathon = Marathon.current
    @participant = @marathon.participants.build(participant_params)
    @participant.confirmed = false
    if @participant.save
      respond_to do |format|
        format.js
      end
    else
      respond_to do |format|
        format.json { render json: @participant.errors.full_messages.to_sentence, status: :unprocessable_entity }
        format.html { redirect_to root_path }
      end
    end
  end

  def confirm
    @participant = Participant.find_by code:(params[:code])
    if @participant
      @participant.confirm!
      redirect_to thanks_path
    else
      render text: "User is not found."
    end
  end

  private

    def participant_params
      params.require(:participant).permit(:name, :email, :toc, :amount, :confirmed, :marathon_id, :payment_type, :monthly_payments, :months)
    end

end
