class ProjectsController < ApplicationController

  def index
    @projects = Project.by_completed
  end

  def new_payment
    @project = Project.find(params[:id])
    @liqpay_request = Liqpay::Request.new(
      amount:      params[:amount].presence || "20",
      currency:    'UAH',
      type:        'donate',
      order_id:    @project.order_id,
      description: params[:name].presence || 'анонім',
      result_url:  project_url(@project),
      server_url:  liqpay_payment_project_url(@project)
    )
    respond_to do |page|
      page.js {}
    end
  end

  def show
    @project = Project.find(params[:id])
  end

end
