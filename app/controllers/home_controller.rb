class HomeController < ApplicationController
  layout 'layouts/welcome', only: :thanks

  def index
    @marathon = Marathon.active_or_timed
    @reports = Report.ordered
  end

  alias_method :v2, :index

  def thanks
  end
end
