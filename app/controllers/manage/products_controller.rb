class Manage::ProductsController < Manage::ManageController
  inherit_resources
  actions :all, :except => :show
  defaults :resource_class => Product

  def index
    @products = Product.page(params[:page]).per(30)
  end

  private
  def permitted_params
    params.permit!
  end
end
