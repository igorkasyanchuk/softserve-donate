class Manage::PagePartsController < Manage::ManageController
  inherit_resources
  before_action :set_page_part, only: [:edit, :update, :destroy, :show]

  def index
    @page_parts = PagePart.all
  end

  def show
  end

  def new
    @page_part = PagePart.new
  end

  def edit
  end

  def create
    @page_part = PagePart.new(page_part_params)
    if @page_part.save
      redirect_to [:manage, @page_part], notice: 'Page part was successfully created.'
    else
      render :new
    end
  end

  def update
    if @page_part.update(page_part_params)
      redirect_to [:manage, @page_part], notice: 'Page part was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @page_part.destroy
    redirect_to manage_page_parts_url, notice: 'Page part was successfully destroyed.'
  end

  private
    def set_page_part
      @page_part = PagePart.find(params[:id])
    end

    def page_part_params
      params.require(:page_part).permit(:identifier, :content_en, :content_uk, :format, :content_en_plain, :content_uk_plain, :content_en_html, :content_uk_html)
    end
end
