class Manage::CategoriesController < Manage::ManageController
  inherit_resources
  actions :all, :except => :show
  defaults :resource_class => Category

  def index
    @categories = Category.page(params[:page]).per(30)
  end

  private
  def permitted_params
    params.permit(:category => [:name])
  end
end
