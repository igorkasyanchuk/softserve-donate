class Manage::ManageController < ApplicationController
  before_action :authenticate_user!

  before_action :set_time_zone_to_en

  inherit_resources

  layout 'admin'
  
  def index
  end

  private

  def set_time_zone_to_en
    I18n.locale = :en
  end

end