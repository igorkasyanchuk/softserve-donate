class Manage::MarathonsController < Manage::ManageController
  inherit_resources
  actions :all, :except => :show
  defaults :resource_class => Marathon

  def index
    @marathons = Marathon.page(params[:page]).per(30)
  end

  def participants
    @marathon     = Marathon.find(params[:id])
    @participants = @marathon.participants.page(params[:page]).per(30)
  end

  def participants_csv
    @marathon     = Marathon.find(params[:id])
    @participants = @marathon.participants

    respond_to do |format|
      format.csv { render csv: @participants, filename: 'participant-list.csv' }
    end
  end

  private
  def permitted_params
    params.permit!
  end
end
