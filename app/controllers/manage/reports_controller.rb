class Manage::ReportsController < Manage::ManageController
  inherit_resources
  defaults resource_class: Report

  def index
    @reports = Report.page(params[:page]).per(20)
  end

  def new
    @report = Report.new
  end

  def edit
    @report = Report.find(params[:id])
    session[:return_to] ||= request.referer
  end

  def city_reports
    @report = Report.find(params[:id])
    session[:return_to] ||= request.referer
  end

  def create
    @report = Report.new(report_params)
    if @report.save
      redirect_to manage_reports_path, notice: 'Report was successfully created.'
    else
      render :new
    end
  end

  def update
    update! {[:manage, :reports]}
  end

  private

  def report_params
    params.require(:report).permit(
      :id, :final_number_of_people, :report_month, :money_uah, :money_usd, :description_en,
      :description_uk, :_destroy, :name_uk, :name_en, :end_month, :bucket,
      sub_reports_attributes: [
        :id, :amount, :short_description_uk, :short_description_en, :_destroy, :order_id, :currency,
        :image, :image_cache, :section_en, :section_uk, :with_section,
        report_tables_attributes: [:id, :name_uk, :name_en, :amount_en, :amount_uk, :price, :_destroy],
        images_attributes: [:id, :image, :youtube_url, :asset_type, :_destroy]
      ],
      city_reports_attributes: [
        :id, :city, :content_en, :content_uk, :_destroy, :image_cache, :currency,
        images_attributes: [:id, :description_en, :description_uk, :image, :youtube_url, :asset_type, :image_cache, :_destroy],
        report_tables_attributes: [:id, :name_uk, :name_en, :amount_en, :amount_uk, :price,:_destroy]
      ]
    )
  end
end
