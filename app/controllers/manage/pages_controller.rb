# -*- encoding : utf-8 -*-
class Manage::PagesController < Manage::ManageController
  inherit_resources
  actions :all, :except => :show
  defaults :resource_class => Page

  def index
    @pages = Page.page(params[:page]).per(20)
  end

  def new
    @page = Page.new
    @parent = Page.find_by_id(params[:parent])
  end

  def edit
    @page = Page.find(params[:id])
    @parent = @page.parent
    session[:return_to] ||= request.referer
  end

  def create
    create! {[:manage, :pages]}
  end

  def update
    update! {[:manage, :pages]}
  end

  private

  def page_params
    params.require(:page).permit(:content, :parent_id, :seo_title, :seo_keywords, :seo_description, :priority, :position)
  end

end
