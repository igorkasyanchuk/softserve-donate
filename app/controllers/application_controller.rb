require 'csv'

class ApplicationController < ActionController::Base
  before_action :set_locale
  after_filter :prepare_unobtrusive_flash
  protect_from_forgery with: :exception

  protected

  def ckeditor_filebrowser_scope(options = {})
    super({ :assetable_id => current_user.id, :assetable_type => 'User' }.merge(options))
  end

  def after_sign_in_path_for(resource)
    manage_marathons_path
  end

  private

  def default_url_options(options={})
    { locale: I18n.locale }
  end

  def set_locale
    params[:locale] = 'uk' if params[:locale] == 'ua'
    locale = params[:locale] || I18n.default_locale
    redirect_to '/' and return unless ['uk', 'en'].include?(locale.to_s)
    I18n.locale = locale
  end

end