class ReportsController < ApplicationController
  def show
    @report = Report.find(params[:id])
    @reports = Report.ordered
  end
end
