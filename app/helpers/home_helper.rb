module HomeHelper
  require 'time_diff'

  YT_DEFAULTS = {
    showControls: false,
    containment: 'body',
    autoPlay: true,
    videoURL: '',
    opacity: 1,
    startAt: 0,
    stopAt: 0,
    loop: true,
    mute: true
  }.freeze

  def background_video_properties(options = {})
    YT_DEFAULTS.dup.merge(options)
  end

  def yt_properties_parts
    parts = PagePart.where('`identifier` LIKE ?', '%youtube_bg_video%')
    options = Hash[parts.map { |e| [e.identifier.to_sym, e.content.to_s] }]
    {
      videoURL: options[:youtube_bg_video_url],
      startAt: options[:youtube_bg_video_start_at].to_i,
      stopAt: options[:youtube_bg_video_stop_at].to_i
    }
  end

  def page_part(identifier)
    pp = PagePart.find_or_create_by(identifier: identifier)
    pp.format.eql?('html') ? pp.content.to_s.html_safe : pp.content.to_s
  end

  def progress_image(progress)
    image_tag("grafik/#{progress}.png", class: 'percentage-img')
  end

  def time_left(date_to, date_from = Time.zone.now)
    time_diff_components = Time.diff(date_from, date_to, '%d')
    left_time = distance_of_time_in_words(date_from, date_to)
    if %i( year month day week ).any? { |e| time_diff_components[e] > 0 }
      content_tag(:div, class: 'days-block') do
        image_tag('clock_yellow.png', class: 'clock') +
        content_tag(:span, "#{left_time}", class: 'yellow')
      end
    else
      content_tag(:div, :class=>'days-block') do
        image_tag('clock_red.png', class: 'clock') +
        content_tag(:span, "#{time_diff_components[:hour].to_s.rjust(2, '0')}:#{time_diff_components[:minute].to_s.rjust(2, '0')}", class: 'last-day')
      end   
    end
  end


def time_to_next_sprint(date_to, date_from = Time.zone.now)
  seconds_diff = (date_to - date_from).to_i.abs
  days = seconds_diff / (24*3600)
  seconds_diff -= days * 3600 * 24
  hours = seconds_diff / 3600
  seconds_diff -= hours * 3600
  minutes = seconds_diff / 60

  content_tag(:div, :class=>'col-xs-4') do
    content_tag(:div , "#{days.to_s.rjust(2, '0')}", class: 'yellow strong') +
    content_tag(:span, "#{t :days}", class: 'yellow')
  end +
  content_tag(:div, :class=>'col-xs-4') do
    content_tag(:div , "#{hours.to_s.rjust(2, '0')}", class: 'yellow strong') +
    content_tag(:span, "#{t :hours}", class: 'yellow')
  end +
  content_tag(:div, :class=>'col-xs-4') do
    content_tag(:div , "#{minutes.to_s.rjust(2, '0')}", class: 'yellow strong') +
    content_tag(:span, "#{t :minutes}", class: 'yellow')
  end  
end

  def insert_current_month(string)
    string.gsub(/XXXXXX/, l(Date.current, format: :month))
  end
end
