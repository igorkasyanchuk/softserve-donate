module ApplicationHelper

  def highlight(path)
    "active" if current_page?(path)
  end

  def permitted?(obj)
    case obj
    when Project
      current_user && current_user.permitted_projects.include?(obj)
    when Event
      current_user && current_user.permitted_events.include?(obj)
    else
      false
    end
  end

  def page_meta(opt={})
    # https://developers.facebook.com/docs/sharing/best-practices#tags
    # http://schema.org/
    #
    # og:title
    #     The title of your article, excluding any branding.
    # og:site_name
    #     The name of your website. Not the URL, but the name. (i.e. "IMDb" not "imdb.com".)
    # og:url
    #     This URL serves as the unique identifier for your post.
    #     It should match your canonical URL used for SEO, and it should not include any session variables, user identifying parameters, or counters.
    #     If you use this improperly, likes and shares will not be aggregated for this URL and will be spread across all of the variations of the URL.
    # og:description
    #     A detailed description of the piece of content, usually between 2 and 4 sentences.
    #     This tag is technically optional, but can improve the rate at which links are read and shared.
    # og:image
    #     This is an image associated with your media. We suggest that you use an image of at least 1200x630 pixels.
    # fb:app_id
    #     The unique ID that lets Facebook know the identity of your site.
    #     This is crucial for Facebook Insights to work properly. Please see our Insights documentation to learn more.

    image_arr = []
    image_arr << image_url('img-bird.png') # add default image

    opt_def = {
        title:        'default_title',
        site_name:    'Заради Життя',
        url:          'www.forlife.if.ua/',
        description:  'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
        image:        image_arr,
        app_id:       ''
    }
    opt_def.merge!(opt)

    og_array = []
    schema_array = []

    schema_array << "<meta itemprop='name'  content='#{opt_def[:title]}' />"

    opt_def.each_pair do |key, val|
      unless val == ''
        if val.kind_of?(Array)
          val.each do |e|
            og_array      << "<meta property='og:#{key}'  content='#{e}' />"
            schema_array  << "<meta itemprop='#{key}'  content='#{e}' />"
          end
        else
          og_array        << "<meta property='og:#{key}' content='#{val}' />"
          schema_array    << "<meta itemprop='#{key}'  content='#{val}' />"
        end
      end
    end

    content_for(:page_meta) { og_array.join(" ").html_safe }
    content_for(:page_meta) { schema_array.join(" ").html_safe }

  end

  def full_image_url(img_url)
    request.protocol + request.host_with_port + img_url.to_s
  end

  def title(page_title)
    content_for(:title) { page_title }
  end

  def admin_menu?(item)
    case item
    when :contacts
      controller_name == 'contacts'
    when :content
      controller_name == 'page_parts'
    when :pages
      controller_name == 'pages'
    when :categories
      controller_name == 'categories'
    when :sites
      controller_name == 'sites'
    when :organizations
      controller_name == 'organizations'
    when :users
      controller_name == 'users'
    when :projects
      controller_name == 'projects'
    when :events
      controller_name == 'events'
    when :marathons
      controller_name == 'marathons'
    when :products
      controller_name == 'products'
    when :reports
      controller_name == 'reports'
    end
  end

  def yes_no(e)
    e ? 'yes' : 'no'
  end

  def active_locale?(locale)
    I18n.locale == locale
  end

  def bool_to_value(bool, hash = {})
    hash[bool]
  end
end
