$(document).ready ->

  $(".player").mb_YTPlayer()

  $('input:checkbox').on 'change', ->
    disable_button_handler()

  $("#new_participant").on "ajax:error", (e, data, status, xhr) ->
    $('.js-form-error').html data.responseText

  $('#scroll-top').on 'click', ->
    $("html, body").animate({ scrollTop: 0 }, "slow");

  if $('body').height() < $(document).height()
    $(document).on 'click', '.scroll_arrow', ->
      $("html, body").animate({ scrollTop: $(document).height() }, "slow")
  else
    $('.scroll_arrow').hide()

  $('input:radio').on 'change', ->
     disable_button_handler()
     if $(this).attr('id') != 'participant_payment_type_other'
       $('#participant_amount').val('');

  $('#participant_amount').keyup ->
    disable_button_handler()

  $('#participant_amount').keyup ->
    $("#participant_payment_type_other").prop("checked", true)

  disable_button_handler = ->
    if ($("#participant_payment_type_other").is(":checked") && $('#participant_amount').val().length == 0) || (!$("#participant_toc").is(":checked"))
      $('.js-participant-button').attr( 'disabled', true )
    else
      $('.js-participant-button').removeAttr( 'disabled' )


  $("#participant_amount").keydown (e) ->

    # Allow: backspace, delete, tab, escape, enter and .
    # Allow: Ctrl+A
    # Allow: home, end, left, right
    # let it happen, don't do anything
    return  if $.inArray(e.keyCode, [
      46
      8
      9
      27
      13
      110
      190
    ]) isnt -1 or (e.keyCode is 65 and e.ctrlKey is true) or (e.keyCode >= 35 and e.keyCode <= 39)

    # Ensure that it is a number and stop the keypress
    e.preventDefault()  if (e.shiftKey or (e.keyCode < 48 or e.keyCode > 57)) and (e.keyCode < 96 or e.keyCode > 105)

  $("[data-toggle=\"popover\"]").popover()


  $('.jcarousel').jcarousel({});

  $(".city-report-images img").on 'click', ->
    slide = $(this).data("slide-to")
    $('.carousel').carousel(slide);

  $(".monthly_payments_cb span").on 'click', ->
    $("#participant_monthly_payments").trigger('click')

