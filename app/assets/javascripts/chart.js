function drawChart (data) {
  // Create the chart
  $('#chart').highcharts('StockChart',{
    xAxis: {
      minRange: 3600000
    },
    rangeSelector : {
      selected : 7,
      buttons: [
      {
        type: 'day',
        count: 1,
        text: '1d'
      },
      {
        type: 'week',
        count: 1,
        text: '1w'
      },
      {
        type: 'month',
        count: 1,
        text: '1m'
      }, {
        type: 'month',
        count: 3,
        text: '3m'
      }, {
        type: 'month',
        count: 6,
        text: '6m'
      }, {
        type: 'ytd',
        text: 'YTD'
      }, {
        type: 'year',
        count: 1,
        text: '1y'
      }, {
        type: 'all',
        text: 'All'
      }]
    },

    title : {
      text : 'Participant per Hour'
    },

    series : [{
      name : 'N',
      data : data,
    }]
  });
}



