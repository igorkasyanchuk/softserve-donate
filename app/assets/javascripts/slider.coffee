_run_jcarousel = ->
  jcarousel = $('.jcarousel')

  jcarousel.on 'jcarousel:reload jcarousel:create', ->
    width = jcarousel.innerWidth()
    if(width >= 600 || width >= 350)
      width = width / 2
    jcarousel.jcarousel('items').css('width', width + 'px')

  jcarousel.jcarousel()

  $('.jcarousel-control-prev').jcarouselControl
    target: '-=1'

  $('.jcarousel-control-next').jcarouselControl
    target: '+=1'

  $('.jcarousel-pagination').on('jcarouselpagination:active', 'a', ->
    $(this).addClass('active')
  ).on('jcarouselpagination:inactive', 'a', ->
    $(this).removeClass('active')
  ).on('click', (e) ->
    e.preventDefault()
  ).jcarouselPagination(
    perPage: 1,
    item: (page) ->
      return '<a href="#' + page + '">' + page + '</a>'
  )

  # Slick slider
  $(".slider-reports").slick
    dots: false
    infinite: true
    speed: 300
    slidesToShow: 2
    responsive: [ { breakpoint: 768, settings: { rows: 1, slidesPerRow: 2 } } ]

  $(".bucket-slider").slick
    infinite: true
    vertical: true

$(document).ready(_run_jcarousel)
