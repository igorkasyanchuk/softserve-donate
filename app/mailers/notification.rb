class Notification < ActionMailer::Base
  add_template_helper(ApplicationHelper)
  add_template_helper(HomeHelper)

  default from: "no-reply-donation@softserveinc.com"

  def confirm(participant)
    @participant = participant
    mail to: participant.email, subject: "SoftServe Donation"
  end

  def confirmed(participant)
    @participant = participant
    mail to: participant.email, subject: "SoftServe Donation"
  end

end
