class OrderMailer < ActionMailer::Base
  default from: "from@example.com"

  def send_mail_to_admin(contact)
    @contact = contact
    @email = Site.current.contact_email
    mail(to: @email, subject: 'New order')
  end

end
