Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'

  scope '(:locale)' do
    namespace :manage do
      root to: 'manage#start', as: :manage
      resources :pages
      resources :page_parts
      resources :products
      resources :reports do
        member do
          get :city_reports
        end
      end
      resources :marathons do
        member do
          get :participants
          get :participants_csv
        end
      end
    end

    devise_for :users

    resources :pages, only: [:show]
    resources :participants, only: [:create] do
      member do
        get :confirm
      end
    end
    resources :reports, only: [:show]

    get '/thank-you', to: 'home#thanks', as: :thanks
    get '/v2', to: 'home#v2', as: :v2

    root 'home#index'
  end
end
