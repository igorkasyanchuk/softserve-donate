lock '3.1.0'

set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, '2.1.5'
#set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value

set :application, 'Donate'
set :repo_url, 'git@bitbucket.org:igorkasyanchuk/softserve-donate.git'
set :deploy_to, '/home/deployer/donate'
set :branch, 'master'
set :scm, :git
set :format, :pretty
set :keep_releases, 5
set :default_env, { rvm_bin_path: '~/.rvm/bin' }
set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets public/uploads public/system}
set :format, :pretty
set :log_level, :info

desc 'Symlink linked directories'
task :linked_dirs do
  next unless any? :linked_dirs
  on release_roles :all do
    execute :mkdir, '-pv', linked_dir_parents(release_path)

    fetch(:linked_dirs).each do |dir|
      target = release_path.join(dir)
      source = shared_path.join(dir)
      unless test "[ -L #{target} ]"
        if test "[ -d #{target} ]"
          execute :rm, '-rf', target
        end
        execute :ln, '-s', source, target
      end
    end
  end
end

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  namespace :assets do
    desc 'Reprocess images'
    task :reprocess do
      on roles(:app) do
        within release_path do
          execute :rake, 'assets:images:reprocess'
        end
      end
    end
  end

  namespace :db do
    desc 'Rails migrate'
    task :migrate do
      on roles(:app) do
        within release_path do
          execute :rake, 'db:migrate'
        end
      end
    end
  end
end

after 'deploy', 'deploy:db:migrate'
# after 'deploy', 'deploy:assets:reprocess'
after 'deploy', 'deploy:restart'
